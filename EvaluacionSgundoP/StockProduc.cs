﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EvaluacionSgundoP
{
    class StockProduc
        {
        public StockProduc()
        {
            this.ListaStockProductos = new List<Producto>();
        }

        public List<Producto> ListaStockProductos { get; set; }

        public void CrearProductos()
        {
            Random random = new Random();
            int numero;
            for (int i = 1; i <= 10; i++)
            {

                Producto producto = new Producto();
                producto.SetDescripcion(i);
                numero = random.Next(20);
                char letra = (char)(((int)'A') + random.Next(26));
                producto.SetDescripcion("PRODUCTO" + letra + numero + i);
                producto.Precio = numero;

                Random existencia = new Random();
                producto.Existencia = existencia.Next(100);
                this.ListaStockProductos.Add(producto);
            }
        }

        //Modificar este método
        public void ImprimirStockProductos()
        {
           // Console.WriteLine("Stock de Productos");
            //Console.WriteLine("Identificador\tDescripción\tPrecio");
          //  foreach (var item in this.ListaStockProductos)
            //{
              //Console.WriteLine("{0}\t\t{1}\t{2}",
           // / item.Identificador, item.Descripcion, item.Precio, item.Existencia);
             //}
          //  var this.ListaStockProductos = existecia.Where(x => x. >= 5).ToList()

            IEnumerable<Producto> ProducQuery =
                 from ListaS in ListaStockProductos
                 select ListaS;

            Console.WriteLine(ProducQuery.OrderBy(x => x.Precio));


            IEnumerable<string> desQuery =
                             from ListaS in ListaStockProductos
                             where ListaS.Descripcion == "HOLA"
                             select ListaS.Descripcion;
            Console.WriteLine(desQuery);
            var sumaProduc = ListaStockProductos.Sum(x => x.Existencia);
            var notaMaxima = ListaStockProductos.Max(x => x.Precio);

        }

    }
}
