﻿using System;

namespace EvaluacionSgundoP
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Console.WriteLine("Carrito de compras de tienda Amazon.");

            StockProduc stock = new StockProduc();
            stock.CrearProductos();
            stock.ImprimirStockProductos();

            // Cliente cliente = new Cliente();
            //cliente.Nombres = "Michael Jackson Zambrano Zambrano";
            // cliente.Email = "michael@web.com";
            // cliente.Cedula = "1112223334";
            // cliente.Contrasena = "mzambrano";

            Factura factura = new Factura();

            string opcion;
            int codigo;
            do
            {
               Console.WriteLine("Ingrese el codigo del producto");
               codigo = int.Parse(Console.ReadLine());
               Producto producto = new Producto();

               Console.WriteLine("Ingrese la cantidad del producto elegido:");
                producto.Existencia = int.Parse(Console.ReadLine());
               factura.Detalle.Add(producto);
              Console.WriteLine("Escriba A para seguir agregando productoso S para salir");
                opcion = Console.ReadLine();
            } while (opcion != "S");

            //IMPRIMIR POR PANTALLA EL NOMBRE DEL PRODUCTO, SU PRECIO Y LA CANTIDAD
            Console.WriteLine("Productos facturados");
            Console.WriteLine("Descripcion\tPrecio\tCantidad");

            foreach (var item in factura.Detalle)
            {
                Console.WriteLine("{0}\t{1}\t{2}",
                    item.GetDescripcion(), item.Precio, item.Existencia);
            }

            Console.WriteLine();

            factura.CalcularSubtotal();
            factura.CalcularDescuento();
            factura.CalcularTotal();

            Console.WriteLine(factura.SubTotal);
            Console.WriteLine(factura.Descuento);
            Console.WriteLine(factura.Total);



        }
    }
}
