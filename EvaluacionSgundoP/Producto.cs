﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EvaluacionSgundoP
{
    class Producto
    {
        private int descripcion;

        // identificador del producto, es un valor único
        public int GetDescripcion()
        {
            return descripcion;
        }

        // identificador del producto, es un valor único
        public void SetDescripcion(int value)
        {
            descripcion = value;
        }

        //  la cantidad de productos que existe en la tienda
        public int Existencia { get; set; }

        //  el nombre o descripcion del producto
        public string Descripcion { get; set; }

        //  el precio del producto.
        public decimal Precio { get; set; }

        internal void SetDescripcion(string v)
        {
            throw new NotImplementedException();
        }
    }
}
