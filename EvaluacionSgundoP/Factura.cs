﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EvaluacionSgundoP
{
    class Factura
    {
        public decimal SubTotal { get; set; }
        public decimal Descuento { get; set; }
        public decimal Total { get; set; }

        public Factura()
        {
            this.Detalle = new List<Producto>();
        }

        //Detalle de carrito
        //Lista de detalle de carrito (Producto y la cantidad)

        public List<Producto> Detalle { get; set; }


        public void CalcularSubtotal()
        {
            //Acumulador o sumador
            decimal subtotal = 0;
            foreach (Producto item in this.Detalle)
            {
                subtotal = subtotal + (item.Existencia* item.Precio);
            }

            this.SubTotal = subtotal;
        }

        public void CalcularDescuento()
        {
            if (this.SubTotal > 100)
            {
                this.Descuento = this.SubTotal * 0.05M;
            }
            else
            {
                this.Descuento = 0;
            }
        }

        public void CalcularTotal()
        {
            this.Total = this.SubTotal - this.Descuento;
        }

    }
}
